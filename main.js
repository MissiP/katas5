//Reverse a string

function reverseString(string) {
    return string.split("").reverse().join("")
}
let string = reverseString("hello")
console.log(string)
console.assert(reverseString("test") === "tset");
console.assert(reverseString("pink") === "knip");


//Reverse a sentence ("bob likes dogs" -> "dogs likes bob")

function reverseSentence(sentence) {
    return sentence.split(" ").reverse().join(" ")
}
let sentence = reverseSentence("bob likes dogs")
console.log(sentence)
console.assert(reverseSentence("bob likes dogs") === "dogs likes bob");


//Find the minimum value in an array

var sumSmall = [469, 755, 244]

function findMin(array) {
    var minValue = array[0]
    for (var i = 0; i < array.length; i++) {
        if (array[i] < minValue) {
            minValue = array[i]
        }
    }
    return minValue
}

let newb = findMin(sumSmall)
console.log(newb)
console.assert(findMin(sumSmall) === 244);


//Find the maximum value in an array

var sumLarge = [469, 755, 244]

function findMax(array) {
    var maxValue = array[0]
    for (var i = 0; i < array.length; i++) {
        if (array[i] > maxValue) {
            maxValue = array[i]
        }
    }
    return maxValue
}

let newMax = findMax(sumLarge)
console.log(newMax)
console.assert(findMax(sumLarge) === 755);

//Calculate a remainder (given a numerator and denominator)

function findRem(denominator, numerator) {
    return numerator % denominator
}
console.log(findRem(4, 2));
console.assert(findRem(4, 2) === 2);

//Return distinct values from a list including duplicates (i.e. "1 3 5 3 7 3 1 1 5" -> "1 3 5 7")

var dupValues = [1, 3, 5, 3, 7, 3, 1, 1]

function duplicates(array) {
    let uniArray = []
    for (var i = 0; i < array.length; i++) {
        if (!uniArray.includes(array[i])) {
            uniArray.push(array[i])
        }

    }
    return uniArray
}

console.log(duplicates(dupValues))
console.assert(JSON.stringify(duplicates(dupValues)) === JSON.stringify([1, 3, 5, 7]))

JSON.stringify()


 //Return distinct values and their counts (i.e. the list above becomes "1(3) 3(3) 5(2) 7(1)")

 var countValues = [1, 3, 5, 3, 7, 3, 1, 1]

function count(array) {
    let countVal = {}
    let finStri = ''
    for (var i = 0; i < array.length; i++) {
        if (!(array[i] in countVal)) {
           countVal[array[i]] = 1
        } else {
            countVal[array[i]]++
        } 

    }

    for (i in countVal){
        finStri += i+'('+countVal[i]+')'+' '
    }
    return finStri
}
console.log(count(countValues))
console.assert(count(countValues) === ('1(3) 3(3) 5(1) 7(1) '))


 //Given a string of expressions (only variables, +, and -) and an object describing a set of variable/value pairs like {a: 1, b: 7, c: 3, d: 14}, return the result of the expression ("a + b+c -d" would be -3)


function yes(eq, object) {
  let a = eq.split(" ")
  for (let i = 0; i < a.length; i++) {
      if (a[i] === '+' || a[i] === '-') {
          a[i] = a[i]
      } else {
          a[i] = "object." + a[i]
      }
  }
  a = a.join(" ")
  let b = a.toString()
  console.log(b)
  console.log(eval(b))
  return eval(b)
}
console.log("8=" + yes("a + b + c - d", { a: 1, b: 7, c: 3, d: 14 }))
console.log("8=" + yes("a + b + c - d + e", { a: 1, b: 7, c: 3, d: 14, e: 3 }))
console.assert(yes("a + b + c - d", { a: 1, b: 7, c: 3, d: 14 }) === -3)

